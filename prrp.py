

########################
# SHOULD BE RUN AS ROOT
########################
from nest.experiment import *
from nest.topology import *
from nest.routing.routing_helper import RoutingHelper
from nest import config

################################################
# Topology
#
#   peer-1 --- router-1 --- router-2 --- peer-2


################################################



### Create Nodes ###


#dummy commit
config.set_value('assign_random_names',False)
config.set_value('delete_namespaces_on_termination',False)

peer1 = Node("peer-1")
peer2 = Node("peer-2")
#peer3 = Node("peer-3")
#peer4 = Node("peer-4")
router1 = Node("router-1")
router2 = Node("router-2")

### Enable ip forwarding in routers ###

router1.enable_ip_forwarding()
router2.enable_ip_forwarding()


### Create interfaces and connect nodes and routers ###

(eth_p1r1, eth_r1p1) = connect(peer1, router1, "eth-p1r1-0", "eth-r1p1-0")
(eth_r1r2, eth_r2r1) = connect(router1, router2, "eth-r1r2-0", "eth-r2r1-0")
(eth_r2p2, eth_p2r2) = connect(router2, peer2, "eth-r2p2-0", "eth-p2r2-0")

### Assign addresses to interfaces ###

eth_p1r1.set_address("10.0.1.1/24")
eth_r1p1.set_address("10.0.1.2/24")

eth_r1r2.set_address("10.0.2.2/24")
eth_r2r1.set_address("10.0.2.3/24")

eth_r2p2.set_address("10.0.3.3/24")
eth_p2r2.set_address("10.0.3.4/24")

#peer1.add_route('DEFAULT',eth_p1r1)
#router1.add_route('DEFAULT',eth_r1r2)
#router2.add_route('DEFAULT',eth_r2r1)
#peer2.add_route('DEFAULT',eth_p2r2)

RoutingHelper(protocol='rip').populate_routing_tables()



### Add bandwidth, delay and qdisc at interface ###



eth_p1r1.set_attributes("100mbit", "5ms")
eth_r1p1.set_attributes("100mbit", "5ms")

eth_p2r2.set_attributes("100mbit", "5ms")
eth_r2p2.set_attributes("100mbit", "5ms")

# Bottleneck link
eth_r1r2.set_attributes("10mbit", "40ms", "codel")
eth_r2r1.set_attributes("10mbit", "40ms", "pie")

### Add experiment to run ###
peer1.ping(eth_p2r2.address, True)
print("<------- 1st Ping is Completed  ---------> ")
peer2.ping(eth_p1r1.address, True)
print("<------- 2nd Ping is Completed  ---------> ")

# 'Flow objects' to be added to relevant experiments
flow1 = Flow(peer1, peer2, eth_p2r2.address, 0, 10, 1)

exp1 = Experiment("tcp_4up")
exp1.add_flow(flow1)

### Run the experiment! ###
exp1.run()